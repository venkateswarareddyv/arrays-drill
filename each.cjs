function each(elements, cb){
    if(typeof elements !=="object" || typeof cb !== "function"){
        return elements;
    }
    let newArray=[]
    for(let keyorIndex in elements){
        let obtainedValue=cb(elements[keyorIndex]);
        newArray.push(obtainedValue);
    }
    return newArray;
}

module.exports=each;
