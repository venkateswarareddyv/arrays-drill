function filter(elements, callback) {
    if (!Array.isArray(elements) || typeof elements!=="object" || typeof callback!=="function"){
       return [];
    }
    let arrays = []

    for (let index of elements){
        if (callback(index)){
            arrays.push(index)
        }
    }
    return arrays;
}

module.exports = filter;
