function find(elements, cb) {
    if (!Array.isArray(elements) || elements.length===0 || typeof cb!=="function") {
        return undefined
    }
    for(let i=0;i<elements.length;i++){
        let obtainedValue=cb(elements[i])
        if(obtainedValue){
            return elements[i]
        }
    }
    return undefined;
}
module.exports = find;
