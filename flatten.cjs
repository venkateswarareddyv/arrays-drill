let newArray=[]
function flatten(elements) {
    if(!Array.isArray(elements) || elements.length===0){
        return []
    }
    for(let i=0;i<elements.length;i++){
        if(Array.isArray(elements[i])){
            flatten(elements[i])
        }else{
            newArray.push(elements[i])
        }
    }
    return newArray;
}

module.exports=flatten;