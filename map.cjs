function map(elements, cb){
  if(typeof elements !== 'object' || typeof cb !== 'function') {
    return [];
  }
  let newArray=[]
  for(let keyOrIndex in elements)
  {
      let obtainedElement=cb(elements[keyOrIndex], keyOrIndex);
      newArray.push(obtainedElement)
  }
  return newArray;
}

module.exports = map;
