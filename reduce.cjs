function reduce(elements, cb, startingValue=null) {
    if(!Array.isArray(elements) || typeof cb !== 'function') {  
      return undefined;
    }
    let i = 0;
    if(startingValue === null) {
      startingValue = elements[0];
      i++;
    }
    for(i; i<elements.length;i++) {
        startingValue = cb(startingValue, elements[i])
    } 
    return startingValue;
}

module.exports=reduce;