let eachFunction=require('../each.cjs')

const items = [1, 2, 3, 4, 5, 5]; 

let callbackFunction=(element)=>{
    return element*10;
}

let result=eachFunction({a:1},callbackFunction);

console.log(result);
