let reduceFunction=require('../reduce.cjs')

const items = [1, 2, 3, 4, 5, 5,5]; 

let callbackFunction=(start,element)=>{
    return start+element;
}

let initialValue=0;

let result=reduceFunction(items,callbackFunction,initialValue);

console.log(result)